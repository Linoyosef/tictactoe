import { Children, JSXElementConstructor, useState } from "react";
import "./App.css";

function Square({
  value,
  onClickSquare,
}: {
  value: string;
  onClickSquare: () => void;
}) {
  return (
    <button className="square" onClick={onClickSquare}>
      {" "}
      {value}
    </button>
  );
}

interface boradProps {
  xIsNext: boolean;
  squers: string[];
  onPlay: (nextSquers: string[]) => void;
}

const SquersLine = (prop: { index: number ,squers:string[], handleClick: (index:number)=>{}}) => {
  return (
    <div className="board-row">
      <Square
        value={prop.squers[prop.index]}
        onClickSquare={() => prop.handleClick(prop.index)}
      />
      <Square
        value={prop.squers[prop.index + 1]}
        onClickSquare={() => prop.handleClick(prop.index + 1)}
      />
      <Square
        value={prop.squers[prop.index + 2]}
        onClickSquare={() => prop.handleClick(prop.index + 2)}
      />
    </div>
  );
};

const BoardItems = (props:{handleClick:(index:number)=>{}, squers: string[]}) => {
  return (
    <>
      {Array.from({ length: 3 }).map((_, i) => (
        <SquersLine index={i * 3} squers={props.squers} handleClick={()=> props.handleClick(i)}/>
      ))}
    </>
  );
};

function Board(props: boradProps) {
  let handleClick = (index: number) => {
    if (props.squers[index] || calcWin(props.squers)) {
      return;
    }
    const nextSquers = props.squers.slice();

    nextSquers[index] = props.xIsNext ? "X" : "O";

    props.onPlay(nextSquers);
  };

  const winner = calcWin(props.squers);
  let status = winner
    ? "Winner: " + winner
    : "Next player: " + (props.xIsNext ? "X" : "O");


  return (
    <>
      <BoardItems handleClick={()=>handleClick} squers={props.squers}/>
      <div className="status">{status}</div>
    </>
  );
}

export default function Game() {
  const [history, setHistory] = useState([Array(9).fill(null)]);
  const [currentMove, setCurrentMove] = useState(0);
  const currentSquers = history[currentMove];
  const xIsNext = currentMove % 2 === 0;

  function handlePlay(nextSquers: string[]) {
    const nextHistory = [...history.slice(0, currentMove + 1), nextSquers];
    setHistory(nextHistory);
    setCurrentMove(nextHistory.length - 1);
  }

  function jumpTo(nextMove: number) {
    setCurrentMove(nextMove);
  }

  const moves = history.map((squers, move) => {
    let description;
    if (move > 0) {
      description = "go to  #" + move;
    } else {
      description = "go to game start";
    }

    return (
      <li key={move}>
        <button onClick={() => jumpTo(move)}>{description}</button>
      </li>
    );
  });

  return (
    <div className="game">
      <div className="game-board">
        <Board xIsNext={xIsNext} squers={currentSquers} onPlay={handlePlay} />
      </div>
      <div className="game-info">
        <ol>{moves}</ol>
      </div>
    </div>
  );
}

function calcWin(squers: string[]) {
  const winnig = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < winnig.length; i++) {
    const [a, b, c] = winnig[i];
    if (squers[a] && squers[a] === squers[b] && squers[a] === squers[c]) {
      return squers[a];
    }
  }
  return null;
}
